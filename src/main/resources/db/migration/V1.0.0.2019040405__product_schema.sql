create table product (
  id int AUTO_INCREMENT,
  nama_barang varchar (40) not null,
  harga int not null,
  brand varchar(50) not null,
  kategori varchar(50) not null,
  sub_kategori varchar(50) not null,
  primary key (id)
);

