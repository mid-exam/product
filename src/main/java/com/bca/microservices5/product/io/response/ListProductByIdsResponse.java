package com.bca.microservices5.product.io.response;

import java.util.List;

import com.bca.microservices5.product.entity.Product;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper=false)
@Data
public class ListProductByIdsResponse extends BaseResponse{

	private List<Product> products;
}
