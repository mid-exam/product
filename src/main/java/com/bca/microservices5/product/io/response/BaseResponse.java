package com.bca.microservices5.product.io.response;

import lombok.Data;

@Data
public class BaseResponse {

	protected String response_code;
	protected String message;
	
}
