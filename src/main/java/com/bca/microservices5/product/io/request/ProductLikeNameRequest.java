package com.bca.microservices5.product.io.request;

import lombok.Data;

@Data
public class ProductLikeNameRequest {
	
	private String name;
	private String orderBy;
}
