package com.bca.microservices5.product.io.request;

import lombok.Data;

@Data
public class ProductFilterRequest {

	private String brand;
	private String kategori;
	private String sub_kategori;
}
