package com.bca.microservices5.product.io.request;

import java.util.List;

import lombok.Data;

@Data
public class ListProductByIdsRequest {
	
	private List<Integer> listProductId;
	
}
