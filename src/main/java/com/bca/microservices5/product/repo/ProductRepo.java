package com.bca.microservices5.product.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.bca.microservices5.product.entity.Product;

@Repository
public interface ProductRepo extends PagingAndSortingRepository<Product, Integer> {

	@Query(nativeQuery = true)
	List<Product> queryMultipleCriteria(String brand,String kategori,String sub_kategori);

	List<Product> findByNamaContaining(String name);
	

}
