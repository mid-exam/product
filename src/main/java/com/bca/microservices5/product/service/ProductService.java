package com.bca.microservices5.product.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bca.microservices5.product.entity.Product;
import com.bca.microservices5.product.repo.ProductRepo;

@Service
public class ProductService {

	@Autowired
	private ProductRepo productRepo;
	
	public List<Product> listProductByIds(Iterable<Integer> listIds){
		
		List<Product> products = new ArrayList<Product>();
		
		products = (List<Product>) productRepo.findAllById(listIds);
		
		return products;
	}

	public List<Product> searchCriteria(String brand, String category, String subCategory) {

		StringBuilder sb = new StringBuilder();
		
		/*
		if(!brand.equals("") && brand!=null) {
			sb.append("brand = '"+brand+"' AND 1");
		}
		if(!category.equals("") && category!=null) {
			
			if(sb.lastIndexOf("1")!=-1) {
				sb.deleteCharAt(sb.lastIndexOf("1"));
			}
			
			sb.append("kategori = '"+category+"' AND 1");
		}
		if(!subCategory.equals("") && subCategory!=null) {
			
			if(sb.lastIndexOf("1")!=-1) {
				sb.deleteCharAt(sb.lastIndexOf("1"));
			}
			
			sb.append("sub_kategori = '"+subCategory+"'");
		}
		*/
		
		if(brand.equals("") || brand==null) {
			brand = "%";
		}
		if(category.equals("") || category==null) {
			category = "%";
		}
		if(subCategory.equals("") || subCategory==null) {
			subCategory = "%";
		}
		
		//System.out.println(sb.toString());
		List<Product> products = productRepo.queryMultipleCriteria(brand,category,subCategory);
				
		return products;
	}
}
