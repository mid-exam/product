package com.bca.microservices5.product.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bca.microservices5.product.entity.Product;
import com.bca.microservices5.product.io.request.ListProductByIdsRequest;
import com.bca.microservices5.product.io.request.ProductFilterRequest;
import com.bca.microservices5.product.io.request.ProductLikeNameRequest;
import com.bca.microservices5.product.io.response.ListProductByIdsResponse;
import com.bca.microservices5.product.repo.ProductRepo;
import com.bca.microservices5.product.service.ProductService;

@RestController
@RequestMapping("/product")
public class ProductController {

	@Autowired
	private ProductRepo productRepo;
	
	@Autowired
	private ProductService productService;
	
	@RequestMapping("/")
	public ListProductByIdsResponse getProduct(@RequestBody ListProductByIdsRequest input){
		ListProductByIdsResponse listProduct = new ListProductByIdsResponse();
		
		listProduct.setResponse_code("00");
		listProduct.setMessage("Berhasil menampilkan data");
		listProduct.setProducts(productService.listProductByIds(input.getListProductId()));
		
		return listProduct;
	}
	
	@RequestMapping("/criteria")
	public Iterable<Product> searchCriteria(@RequestBody ProductFilterRequest input){
		
		List<Product> product = new ArrayList<Product>();
		
		if(input.getBrand()==null||input.getBrand().equals("")) {
			input.setBrand("");
		}
		if(input.getKategori()==null||input.getKategori().equals("")) {
			input.setKategori("");
		}
		if(input.getSub_kategori()==null||input.getSub_kategori().equals("")) {
			input.setSub_kategori("");
		}
		
		product = productService.searchCriteria(input.getBrand(), input.getKategori(), input.getSub_kategori());
		
		return product;
	}
	
	@RequestMapping("/searchName")
	public Iterable<Product> searchLikeName(@RequestBody ProductLikeNameRequest input){
		List<Product> product = new ArrayList<Product>();
		
		product = productRepo.findByNamaContaining(input.getName());
		
		System.out.println(input.getOrderBy());
		
		if(input.getOrderBy().equals("termurah")) {
			Collections.sort(product, Comparator.comparingInt(Product::getHarga).reversed());
		}
		if(input.getOrderBy().equals("termahal")) {
			Collections.sort(product, Comparator.comparingInt(Product::getHarga));
		}

		return product;
	}
	
}
